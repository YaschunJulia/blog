import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

import { User } from '../shared/user';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;
  signInError: boolean = false;

  private userLogin: any = {
    email: '',
    password: ''
  }

  constructor(private userService: UserService,
    private route: ActivatedRoute, 
    private router: Router,
    private af: AngularFire) {}

  ngOnInit() {
    this.route.params.forEach((params: Params)  => {
      if (params['id']){
        this.af.auth.subscribe(auth => {
          if (auth) this.router.navigate(['blog']);
        });
      }
    })
   
  }

  onSignIn(){  
    this.af.auth.login({ email: this.userLogin.email, password: this.userLogin.password }, { provider: AuthProviders.Password, method: AuthMethods.Password })
    .then(() => {
      this.userService.checkUser(this.userLogin.email)
      .subscribe(user => { 
        if (user) {
          this.user = user[0];
            if (this.user.role === 'admin') {
              window.localStorage.setItem('admin_token', `Bearer ${user.id}`);
              this.router.navigate(['admin']);
            } else if (this.user.role === 'user') {
              this.router.navigate(['blog']);
            }
        }          
      })
    })
    .catch( () => {
      this.signInError = true;
      this.userLogin.email = '';
      this.userLogin.password = '';
    })  
  }
}
