import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules} from '@angular/router';

import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { BlogComponent } from './blog/blog.component';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from './shared/auth-guard.service';
import { AuthAdminGuard } from './shared/auth-admin-guard.service';

@NgModule({
  imports: [  
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: 'blog',
        pathMatch: 'full'
      },
      {
        path: 'blog', 
        component: BlogComponent,
      },
      { 
        path: 'admin',
        canActivate: [AuthAdminGuard],
        component: AdminComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [AuthGuard]
      }   
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
