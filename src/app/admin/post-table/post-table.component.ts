import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { Post } from '../../shared/post';
import { PostService } from '../../shared/post.service';

@Component({
  selector: 'app-post-table',
  templateUrl: './post-table.component.html',
  styleUrls: ['./post-table.component.css']
})

export class PostTableComponent implements OnInit {
  posts: Post[];
  source: LocalDataSource;

  settings = {
    actions: {
      add: false
    },
    delete: {
      deleteButtonContent: '<span class="glyphicon glyphicon-trash"></span>',
      confirmDelete: true
    },  
    edit: {
      editButtonContent: '<span class="glyphicon glyphicon-pencil"></span>',
      saveButtonContent: '<span class="glyphicon glyphicon-ok"></span>',
      cancelButtonContent: '<span class="glyphicon glyphicon-remove"></span>',
      confirmSave: true
    },
    pager: {
      perPage: 5
    },
    attr: {
      class: 'table-bordered',
    }, 
    columns: {
      id: {
        title: 'ID',
        editable: false,
        class: 'add-border'
      },
      name: {
        title: 'Post name',
        type: 'string'
      },
      body: {
        title: 'Post body',
        type: 'string'
      },
      author: {
        title: 'Author',
        editable: false
      },
      category: {
        title: 'Category',
        type: 'string'
      },
      createdAt: {
        title: 'Created at',
        editable: false
      },
      updatedAt: {
        title: 'Updated at',
        editable: false
      }
    }
  };

  constructor( private postService: PostService) { 
    this.source = new LocalDataSource();
  }

  ngOnInit() {
    this.initializeSmartTable();
  }

  initializeSmartTable() {
    this.postService.getPosts()
      .subscribe((snapshot) => { 
        this.posts = snapshot.map(item => item.val());
        this.source.load(this.posts);
      })
  }

  onDeleteConfirm(event):void {
    if (window.confirm('Are you sure you want to delete?')) {   
      const id = event.data.id;
      this.postService.deleteUser(id);
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event):void {
    if (window.confirm('Are you sure you want to save?')) {
      this.postService.updateUser(event.newData);
    } else {
      event.confirm.reject();
    }
  }

}
