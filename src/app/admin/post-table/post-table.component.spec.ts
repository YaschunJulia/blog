/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PostTableComponent } from './post-table.component';
import { PostService } from '../../shared/post.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, ConnectionBackend, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { Ng2SmartTableModule, LocalDataSource  } from 'ng2-smart-table';

describe('PostTableComponent', () => {
  let component: PostTableComponent;
  let fixture: ComponentFixture<PostTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostTableComponent],
       providers: [
        {
          provide: Http, useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
          return new Http(backend, defaultOptions);
        }, deps: [BaseRequestOptions]
        },
        {provide: PostService, useClass: PostService},
        {provide: BaseRequestOptions, useClass: BaseRequestOptions},
        {provide: ActivatedRoute, useValue: ActivatedRoute},
        {provide: Router, useValue: Router},
      ],
      imports: [Ng2SmartTableModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
