import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { UserTableComponent } from './user-table/user-table.component';
import { PostTableComponent } from './post-table/post-table.component';
import { CategoryTableComponent } from './category-table/category-table.component';

import { AuthAdminGuard } from '../shared/auth-admin-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'admin',
        canActivate: [AuthAdminGuard],
        component: AdminComponent,
        children: [
          {
            path: '',
            children: [
              { path: 'users', component: UserTableComponent },
              { path: 'posts', component: PostTableComponent },
              // { path: 'posts', component: CategoryTableComponent },
            ]
          }
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {}