import { NgModule } from '@angular/core';

import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';

import { AdminComponent } from './admin.component';
import { UserTableComponent } from './user-table/user-table.component';
import { PostTableComponent } from './post-table/post-table.component';
import { CategoryTableComponent } from './category-table/category-table.component';
import { LoginComponent } from '../login/login.component';

import { AdminRoutingModule } from './admin-routing.module';


@NgModule({
  imports: [
    Ng2SmartTableModule,
    AdminRoutingModule,
  ],
  declarations: [
    AdminComponent, 
    UserTableComponent, 
    PostTableComponent, 
    CategoryTableComponent
  ],
  bootstrap: [AdminComponent]
})
export class AdminModule {}
