import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { User } from '../../shared/user';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  users: User[];
  source: LocalDataSource;

  settings = {
    actions: {
      add: false
    },
    delete: {
      deleteButtonContent: '<span class="glyphicon glyphicon-trash"></span>',
      confirmDelete: true
    },  
    edit: {
      editButtonContent: '<span class="glyphicon glyphicon-pencil"></span>',
      saveButtonContent: '<span class="glyphicon glyphicon-ok"></span>',
      cancelButtonContent: '<span class="glyphicon glyphicon-remove"></span>',
      confirmSave: true
    },
    pager: {
      perPage: 10
    },
    attr: {
       class: 'table-bordered',
    }, 
    columns: {
      id: {
        title: 'ID',
        editable: false
      },
      firstName: {
        title: 'First name'
      },
      lastName: {
        title: 'Last name'
      },
      userName: {
        title: 'User name'
      },
      email: {
        title: 'Email'
      },
      birthYear: {
        title: 'Year of birth',
      },
      country: {
        title: 'Country'
      },
      city: {
        title: 'City'
      },
      biography: {
        title: 'Biography'
      }
    }
  };

  constructor( private userService: UserService) { 
    this.source = new LocalDataSource();
  }

  ngOnInit() {
    this.initializeSmartTable();
  }

  initializeSmartTable() {
    this.userService.getUsers()
      .subscribe((snapshot) => { 
        this.users = snapshot.map(item => item.val());
        this.source.load(this.users);
      })
  }

  onDeleteConfirm(event):void {
    if (window.confirm('Are you sure you want to delete?')) {   
      const id = event.data.id;
      this.userService.deleteUser(id);
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event):void {
    if (window.confirm('Are you sure you want to save?')) {
      this.userService.updateUser(event.newData);
    } else {
      event.confirm.reject();
    }
  }

}
