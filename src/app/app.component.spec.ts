/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './shared/in-memory-data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { AppRoutingModule } from './app-routing.module';
import { BlogRoutingModule } from './blog/blog-routing.module';
import { BlogModule } from './blog/blog.module';
import { AdminModule } from './admin/admin.module';

import { AuthGuard } from './shared/auth-guard.service';
import { AuthAdminGuard } from './shared/auth-admin-guard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy } from '@angular/common';
import { Http, ConnectionBackend, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';

describe('App: Blog', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
     declarations: [
    AppComponent,
    LoginComponent
    ],
    imports: [
      AdminModule,
      BlogModule, 
      FormsModule,
      ReactiveFormsModule,
      InMemoryWebApiModule.forRoot(InMemoryDataService),
      AppRoutingModule,
      BlogRoutingModule
    ],
    providers: [
      {
        provide: Http, useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
        return new Http(backend, defaultOptions);
      }, deps: [BaseRequestOptions]
      },
      {provide: BaseRequestOptions, useClass: BaseRequestOptions},
      {provide: ActivatedRoute, useValue: ActivatedRoute},
      {provide: Router, useValue: Router},
      {provide: Location, useClass: Location},
      {provide: LocationStrategy, useClass: LocationStrategy},
    ]
    });
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
