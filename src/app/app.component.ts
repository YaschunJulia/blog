import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  private isScrolled: boolean;

  constructor() { }

  ngOnInit(): void {   
    this.isScrolled = false;
    this.scrollMove(); 
  }

  scrollMove(): void {
    window.onscroll = () => {
      if (window.pageYOffset > window.innerHeight) {
        this.isScrolled = true;
      } else {
        this.isScrolled = false;
      }
    }
  }

  goAbove(): void { 
     let timer = setInterval(() => (document.body.scrollTop > 0) ? document.body.scrollTop -= 100 : clearInterval(timer) , 50); 
  }
}
