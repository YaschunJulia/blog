import { Injectable, Inject } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { AngularFire } from 'angularfire2';
import { Observable } from 'rxjs/Observable'

import { Post } from './post';

@Injectable()
export class PostService {

  constructor(private af: AngularFire) {}

  getPosts(): any {
    return this.af.database.list('/posts', { preserveSnapshot: true });
  }

  getLimitPosts(key: string, limit: number): any {
    return this.af.database.list('/posts', {
      query: {
        limitToFirst: limit,
        equalTo: key,
        orderByKey: true,
      }
    });
  }

  getNextPost(key: string): any {
    return this.af.database.list('/posts', {
      query: {
        orderByKey: true,
        equalTo: key,
        limitToFirst: 2
      }
    });
  }

  getPost(id: number): any {
    return this.af.database.object(`/posts/${id}`, { preserveSnapshot: true })
  }

  createUser(post: Post) {
    return this.af.database.list('/posts').push(post);
  }

  updateUser(post: Post) {
    return this.af.database.object(`/posts/${post.id}`).update(post);
  }

  deleteUser(id: number) {
    return this.af.database.object(`/posts/${id}`).remove();
  }

}

