import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';

 
@Injectable()
export class AuthAdminGuard implements CanActivate {
 
  constructor(private router: Router) {}

  isSignedInAdmin() {
    return !!window.localStorage.getItem('admin_token');
  }
 
  canActivate() {
    if (!this.isSignedInAdmin()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
