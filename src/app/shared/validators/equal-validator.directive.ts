import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
    providers: [
      { provide: NG_VALIDATORS, useExisting: forwardRef(() => EqualValidator), multi: true }
    ]
})

export class EqualValidator implements Validator {
    constructor(@Attribute('validateEqual') public validateEqual: string,
    @Attribute('reverse') public reverse: string) {
    }

    private get isReverse() {
      if (!this.reverse) return false;
      return this.reverse === 'true' ? true: false;
    }

    validate(c: AbstractControl): { [key: string]: any } {
      let value = c.value;

      let confirmValue = c.root.get(this.validateEqual);

      if (confirmValue && value !== confirmValue.value && !this.isReverse) {
        return {
          validateEqual: false
        }
      }

      if (confirmValue && value === confirmValue.value && this.isReverse) {
        delete confirmValue.errors['validateEqual'];
        if (!Object.keys(confirmValue.errors).length) confirmValue.setErrors(null);
      }

      if (confirmValue && value !== confirmValue.value && this.isReverse) {
        confirmValue.setErrors({ validateEqual: false });
      }

      return null;
    }
}