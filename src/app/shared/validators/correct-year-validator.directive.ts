import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[validateCorrectYear][formControlName],[validateCorrectYear][formControl],[validateCorrectYear][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => CorrectYearValidator), multi: true }
    ]
})

export class CorrectYearValidator implements Validator {
    constructor(@Attribute('validateCorrectYear') public validateCorrectYear: string) {
    }

    validate(control: AbstractControl): { [key: string]: any } {
      let value = control.value;
      let year = new Date(Date.now()).getFullYear();
      
      if (value < year-100 || value > year) {
        return {
          validateCorrectYear: false
        }
      }
      return null;
    }
}
