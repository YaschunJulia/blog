/* tslint:disable:no-unused-variable */

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { EqualValidator} from './equal-validator.directive';
import { dispatchEvent } from '@angular/platform-browser/testing/browser_util';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule, NG_VALIDATORS, AbstractControl, NgForm, FormControl } from '@angular/forms';
import { Component} from '@angular/core';

@Component({
  template: `<form>
              <input type="text" name="password" [ngModel]="password" validateEqual="confirmPassword"  reverse="true">
              <input type="text" name="confirmPassword" [ngModel]="confirmPassword" validateEqual="password"  reverse="false">
             </form>`
})

class TestComponent {}

describe('component: TestComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TestComponent, EqualValidator]
    })
    .createComponent(TestComponent);  
  });


  it('should not validate', async(() => {
    let fixture = TestBed.createComponent(TestComponent);
    let comp = fixture.componentInstance;
    let debug = fixture.debugElement;
    let password = debug.query(By.css('[name=password]'));
    let confirmPassword = debug.query(By.css('[name=confirmPassword]'));
    
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      password.nativeElement.value = '1263';
      confirmPassword.nativeElement.value = '1275';
      dispatchEvent(password.nativeElement, 'input');
      dispatchEvent(confirmPassword.nativeElement, 'input');
      fixture.detectChanges();

      let form: NgForm = fixture.debugElement.children[0].injector.get(NgForm);
      let control = form.control.get('password');
      expect(control.hasError('validateEqual')).toBe(false);
      expect(form.control.valid).toEqual(false);

    });
  }));
  it('should validate', async(() => {
    let fixture = TestBed.createComponent(TestComponent);
    let comp = fixture.componentInstance;
    let debug = fixture.debugElement;
    let password = debug.query(By.css('[name=password]'));
    let confirmPassword = debug.query(By.css('[name=confirmPassword]'));
    
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      password.nativeElement.value = '1234';
      confirmPassword.nativeElement.value = '1234';
      dispatchEvent(password.nativeElement, 'input');
      dispatchEvent(confirmPassword.nativeElement, 'input');
      fixture.detectChanges();

      let form: NgForm = fixture.debugElement.children[0].injector.get(NgForm);
      let control = form.control.get('password');

      expect(form.control.valid).toEqual(true);
    });
  }));
});
