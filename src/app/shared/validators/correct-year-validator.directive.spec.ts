/* tslint:disable:no-unused-variable */

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { CorrectYearValidator} from './correct-year-validator.directive';
import { dispatchEvent } from '@angular/platform-browser/testing/browser_util';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule, NG_VALIDATORS, AbstractControl, NgForm, FormControl } from '@angular/forms';
import { Component} from '@angular/core';

@Component({
  template: `<form>
              <input type="text" [ngModel]="year" name="year" validateCorrectYear="year">
             </form>`
})

class TestYearComponent {}

describe('component: TestYearComponent', () => {
  let component: TestYearComponent;
  let fixture: ComponentFixture<TestYearComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TestYearComponent, CorrectYearValidator]
    })
    .createComponent(TestYearComponent);  
  });


  it('should not validate <1916', async(() => {
    let fixture = TestBed.createComponent(TestYearComponent);
    let comp = fixture.componentInstance;
    let debug = fixture.debugElement;
    let year = debug.query(By.css('[name=year]'));
    
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      year.nativeElement.value = '1230';
      dispatchEvent(year.nativeElement, 'input');
      fixture.detectChanges();

      let form: NgForm = debug.children[0].injector.get(NgForm);
      let control = form.control.get('year');

      expect(control.hasError('validateCorrectYear')).toBe(false);
      expect(form.control.valid).toEqual(false);
    });
  }));

  it('should validate 1916-2016', async(() => {
    let fixture = TestBed.createComponent(TestYearComponent);
    let comp = fixture.componentInstance;
    let debug = fixture.debugElement;
    let year = debug.query(By.css('[name=year]'));
    
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      year.nativeElement.value = '1990';
      dispatchEvent(year.nativeElement, 'input');
      fixture.detectChanges();

      let form: NgForm = debug.children[0].injector.get(NgForm);
      let control = form.control.get('year');

      expect(form.control.valid).toEqual(true);
    });
  }));

  it('should not validate >2016', async(() => {
    let fixture = TestBed.createComponent(TestYearComponent);
    let comp = fixture.componentInstance;
    let debug = fixture.debugElement;
    let year = debug.query(By.css('[name=year]'));
    
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      year.nativeElement.value = '2020';
      dispatchEvent(year.nativeElement, 'input');
      fixture.detectChanges();

      let form: NgForm = debug.children[0].injector.get(NgForm);
      let control = form.control.get('year');

      expect(control.hasError('validateCorrectYear')).toBe(false);
      expect(form.control.valid).toEqual(false);
    });
  }));
});
