/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthAdminGuard} from './auth-admin-guard.service';
import { Router } from '@angular/router';

describe('Service: AuthAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: AuthAdminGuard, useValue: AuthAdminGuard},
        {provide: Router, useValue: Router}
      ]
    });
  });

  it('should ...', inject([AuthAdminGuard, Router], (service: AuthAdminGuard) => {
    expect(service).toBeTruthy();
  }));
});
