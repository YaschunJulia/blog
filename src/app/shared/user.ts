export class User {
    id: string;
    role: string;
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    email: string;
    birthYear: number;
    country: string;
    city: string;
    biography: string;
}