export class Post {
  id: string;
  name: string;
  body: string;
  author: string;
  authorID: string;
  category: string;
  createdAt: string;
  updatedAt: string;
}