/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthGuard } from './auth-guard.service';
import { Router } from '@angular/router';

describe('Service: AuthGuard', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: AuthGuard, useValue: AuthGuard},
        {provide: Router, useValue: Router}
      ]
    });
  });

  it('should ...', inject([Router, AuthGuard], (service: AuthGuard) => {
    expect(service).toBeDefined();
  }));
});
