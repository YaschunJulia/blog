import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AngularFire } from 'angularfire2';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private af: AngularFire, private router: Router) {}
  isActive: boolean = true;

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.af.auth.subscribe((auth) => {
      if (auth && (state.url === '/login' || state.url === '/registration')) {
        this.router.navigate([`/`]);
        this.isActive = false;
      } else {
        this.isActive = true;
      }   
    });
    return this.isActive;
  }
}
