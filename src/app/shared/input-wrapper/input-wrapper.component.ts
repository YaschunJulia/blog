import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-input-wrapper',
  templateUrl: './input-wrapper.component.html',
  styleUrls: ['./input-wrapper.component.css']
})

export class InputWrapperComponent{
  @Input() public fieldName: string = '';
  @Input() public patternErrorMessage: string = '';
  @Input() public form: string = '';

  constructor() {}
}
