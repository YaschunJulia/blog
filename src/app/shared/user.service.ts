import { Injectable, Inject } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

import { User } from './user';

@Injectable()
export class UserService {

  constructor(private af: AngularFire) {}

  getUsers(): any {
    return this.af.database.list('/users', { preserveSnapshot: true });
  }

  getUser(id: string): any {
    return this.af.database.object(`/users/${id}`, { preserveSnapshot: true })
  }

  getUserName(id: string): any {
    return this.af.database.object(`/users/${id}/userName`, { preserveSnapshot: true });
  }

  checkUser(email: string): any {
    return this.af.database.list('/users', {
      query: {
        orderByChild: "email",
        equalTo: email,
      }
    });
  }

  createUser(user: User): string {
    this.af.auth.createUser({email: user.email, password: user.password}).then(() => {
      this.af.auth.login({ email: user.email, password: user.password }, { provider: AuthProviders.Password, method: AuthMethods.Password })
        .catch(function(error) {
          console.log(error.message);
        });
    });
    user.id = this.af.database.list('/users').push(user).key;
    this.updateUser(user);
    return user.id
  }

  updateUser(user: User): any {
    return this.af.database.object(`/users/${user.id}`).update(user);
  }

  deleteUser(id: string) {
    return this.af.database.object(`/users/${id}`).remove();
  }

}
