import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { AppRoutingModule } from './app-routing.module';
import { BlogRoutingModule } from './blog/blog-routing.module';
import { BlogModule } from './blog/blog.module';
import { AdminModule } from './admin/admin.module';

import { AuthGuard } from './shared/auth-guard.service';
import { AuthAdminGuard } from './shared/auth-admin-guard.service';

const myFirebaseConfig = {
  apiKey: "AIzaSyAqq6gSFjtLoEY7d1-WOlSqfXT9OuYEoEA",
  authDomain: "blog-8a0cc.firebaseapp.com",
  databaseURL: "https://blog-8a0cc.firebaseio.com",
  storageBucket: "blog-8a0cc.appspot.com",
  messagingSenderId: "498532677338"
};

const myFirebaseAuthConfig = {
   provider: AuthProviders.Anonymous,
   method: AuthMethods.Anonymous,
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AdminModule,
    BlogModule, 
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BlogRoutingModule,
    AngularFireModule.initializeApp(myFirebaseConfig, myFirebaseAuthConfig)
  ],
  providers: [
    AuthGuard,
    AuthAdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
