/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PostsListComponent } from './posts-list.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { PostService } from '../../shared/post.service';
import { UserService } from '../../shared/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, ConnectionBackend, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';

describe('PostsListComponent', () => {
  let component: PostsListComponent;
  let fixture: ComponentFixture<PostsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostsListComponent],
       providers: [
        {
          provide: Http, useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
          return new Http(backend, defaultOptions);
        }, deps: [BaseRequestOptions]
        },
        {provide: UserService, useClass: UserService},
        {provide: PostService, useClass: PostService},
        {provide: BaseRequestOptions, useClass: BaseRequestOptions},
        {provide: ActivatedRoute, useValue: ActivatedRoute},
        {provide: Router, useValue: Router}
      ],
      imports: [InfiniteScrollModule]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(PostsListComponent);
      component = fixture.componentInstance;
    });
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(PostsListComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should tell ROUTER to navigate when post clicked',
  //     inject([Router], (router: Router) => { // ...

  //     const spy = spyOn(router, 'navigate');

  //     let postEl= fixture.debugElement.query(By.css('.post-title')); 

  //     const navArgs = spy.calls.first().args[0];

  //     const id = component.posts[0].id;
  //     expect(navArgs).toBe('/post/' + id,
  //       'should nav to PostDetail for first post');
  //   }));
});
