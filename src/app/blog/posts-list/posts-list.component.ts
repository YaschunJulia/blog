import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FirebaseObjectObservable } from 'angularfire2';

import { Post } from '../../shared/post';
import { PostService } from '../../shared/post.service';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})

export class PostsListComponent implements OnInit {
  posts : Post[];
  postsLimit : Post[];;
  private isScrolled: boolean;
  private isNext: boolean;
  key: string = null;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private postService: PostService,
    private userService: UserService) {}

  getPosts(): void {
    this.postService.getLimitPosts(this.key, 5).subscribe(posts => {
      if (posts.length) {
        if (!this.posts) {
          this.posts = posts; 
        } else {
          this.postsLimit = posts;
          this.posts = this.posts.concat(this.postsLimit);
        }
        this.key = String(this.posts[this.posts.length - 1].id);
        console.log(this.key);
        this.posts.map(post => {
          this.userService.getUserName(post.authorID).subscribe(snapshot => post.author = snapshot.val());
          post.body = (post.body.length > 300) ? post.body.slice(0,300) + '...' : post.body
        })
      }
    });
  }

  getNext(key: string): void {
    this.postService.getNextPost(key)
      .subscribe(snapshot => { console.log(snapshot);
        this.isNext = true})
  }

  ngOnInit(): void {   
    this.getPosts();
    this.isScrolled = false;
    this.getNext(this.key); 
  }

  goToPostDetail(post: Post): void {
    this.router.navigate(['./post', post.id], { relativeTo: this.route });
  }

  goToUserDetail(post: Post): void {
    this.router.navigate(['./user', post.authorID], { relativeTo: this.route });
  }

  onScroll(): void {
    if (this.isNext) {
      this.getPosts();
      this.getNext(this.key);
    }
  }
}
