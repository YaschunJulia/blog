/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RegistrationComponent } from './registration.component';
import { InputWrapperComponent } from '../../shared/input-wrapper/input-wrapper.component';
import { SelectModule } from 'ng2-select/components/select.module';

import { UserService } from '../../shared/user.service';
import { Http, ConnectionBackend, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistrationComponent, InputWrapperComponent],
       providers: [
        {
          provide: Http, useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
          return new Http(backend, defaultOptions);
        }, deps: [BaseRequestOptions]
        },
        {provide: UserService, useClass: UserService},
        {provide: BaseRequestOptions, useClass: BaseRequestOptions},
        {provide: ActivatedRoute, useValue: ActivatedRoute},
        {provide: Router, useValue: Router}
      ],
      imports: [FormsModule, ReactiveFormsModule, SelectModule]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(RegistrationComponent);
      component = fixture.componentInstance;
    });
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(RegistrationComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
