import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable'
import { AngularFire, AuthMethods, AuthProviders } from 'angularfire2';

import { UserService } from '../../shared/user.service';
import { User } from '../../shared/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})

export class RegistrationComponent implements OnInit{
  users: Observable<any[]>;
  fieldError: string = '';
  countryRequired: boolean = false;
  countryPattern: boolean = false;
  countryValid: boolean = false;

  countries = ['Other', 'USA', 'Ukrain', 'Russian', 'China', 'German', 'French'];

  private user: any = {
    role: 'user',
    firstName: '',
    lastName: '',
    userName: '',
    email: '',
    password: '',
    confirmPassword: '',
    birthYear: '',
    country: '',
    city: '',
    biography: '',
  }

  constructor(private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private af: AngularFire) {}

  getUsers(): void {
    this.users = this.userService.getUsers();
  }  

  ngOnInit() {
    this.getUsers();
  }

  onSubmit(isValid: boolean): void {
    if (this.user.country === '') {
      this.countryRequired = true;
    }
    const fields = Object.keys(this.user);
    fields.forEach(key => {
      if (this.user[key] === '') {
        this.fieldError += key + ', ';
      }
    })
    this.fieldError = (this.fieldError) ? this.fieldError.slice(0, -2) : this.fieldError;
    if (this.fieldError === '' && isValid) {
      delete this.user.confirmPassword;
      let key = this.userService.createUser(this.user);
      if (key) {
        this.user.confirmPassword = this.user.password;
        swal("Good job!", "Your registration finished", "success"); 
        this.router.navigate(['login', {id: key}]);
      } else {
        swal("Oops ...", `Server error` , "error");
      }
    } else {
      swal("Oops ...", `You have mistakes in some fields` , "error");
    }
  }

  selected(value: any): void {
    this.countryPattern = false;
    this.countryRequired = false;
    this.countryValid = true;
    this.user.country = value.text;
  }

  removed(value: any): void {
    this.user.country = '';
    this.countryRequired = true;
    this.countryPattern = false;
    this.countryValid = false;
  }

  typed(value: string): void {
    if (!value.match(/^[A-Za-zА-Яа-яЁё\s]{1,20}$/)) {
      this.countryRequired = false;
      this.countryPattern = true;
      this.countryValid = false;
    } else if (value === ''){
      this.countryRequired = true;
      this.countryPattern = false;
      this.countryValid = false;
    } else {
      this.countryRequired = false;
      this.countryPattern = false;
      this.countryValid = true;
      this.user.country = value;
    }
  }
}
