import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Post } from '../../shared/post';
import { PostService } from '../../shared/post.service';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  post: Post;

  constructor(
    private postService: PostService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = params['id'];
      this.postService.getPost(id)
        .subscribe(snapshot => {
        this.post = snapshot.val();
        this.userService.getUserName(this.post.authorID).subscribe(snapshot => this.post.author = snapshot.val());
        });
    })
    .catch(() => this.post = new Post())
  }

  goToUserDetail(post: Post): void {
    this.router.navigate(['blog/user', post.authorID]);
  }
}
