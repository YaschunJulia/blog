// /* tslint:disable:no-unused-variable */
// import { async, ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { RouterStub, ActivatedRouteStub } from '../../../testing';
// import { PostDetailComponent } from './post-detail.component';

// import { PostService } from '../../shared/post.service';
// import { Post } from '../../shared/post';
// import { Router, ActivatedRoute } from '@angular/router';
// import { Http, ConnectionBackend, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
// import { Location, LocationStrategy } from '@angular/common';

// let activatedRoute: ActivatedRouteStub;
// let comp: PostDetailComponent;
// let fixture: ComponentFixture<PostDetailComponent>;
// let page: Page;

// ////// Tests //////
// describe('HeroDetailComponent', () => {
//   beforeEach(() => {
//     activatedRoute = new ActivatedRouteStub();
//   });
//   describe('when override its provided HeroDetailService', overrideSetup);
// });

// ////////////////////
// function overrideSetup() {
//   class StubHeroDetailService {
//     testPost = new Post(42, 'Test Hero');

//     getHero(id: number | string): Promise<Post>  {
//       return Promise.resolve(true).then(() => Object.assign({}, this.testPost) );
//     }

//     saveHero(hero: Post): Promise<Post> {
//       return Promise.resolve(true).then(() => Object.assign(this.testPost, Post) );
//     }
//   }

//   // the `id` value is irrelevant because ignored by service stub
//   beforeEach(() => activatedRoute.testParams = { id: 99999 } );

//   beforeEach( async(() => {
//     TestBed.configureTestingModule({
//       imports:   [ HeroModule ],
//       providers: [
//         { provide: ActivatedRoute, useValue: activatedRoute },
//         { provide: Router,         useClass: RouterStub},
//         // HeroDetailService at this level is IRRELEVANT!
//         { provide: HeroDetailService, useValue: {} }
//       ]
//     })

//     // Override component's own provider
//     .overrideComponent(HeroDetailComponent, {
//       set: {
//         providers: [
//           { provide: HeroDetailService, useClass: StubHeroDetailService }
//         ]
//       }
//     })

//     .compileComponents();
//   }));

//   let hds: StubHeroDetailService;

//   beforeEach( async(() => {
//     createComponent();
//     // get the component's injected StubHeroDetailService
//     hds = fixture.debugElement.injector.get(HeroDetailService);
//   }));

//   it('should display stub hero\'s name', () => {
//     expect(page.nameDisplay.textContent).toBe(hds.testHero.name);
//   });

//   it('should save stub hero change', fakeAsync(() => {
//     const origName = hds.testHero.name;
//     const newName = 'New Name';

//     page.nameInput.value = newName;
//     page.nameInput.dispatchEvent(newEvent('input')); // tell Angular

//     expect(comp.hero.name).toBe(newName, 'component hero has new name');
//     expect(hds.testHero.name).toBe(origName, 'service hero unchanged before save');

//     click(page.saveBtn);
//     tick(); // wait for async save to complete
//     expect(hds.testHero.name).toBe(newName, 'service hero has new name after save');
//     expect(page.navSpy.calls.any()).toBe(true, 'router.navigate called');
//   }));

//   it('fixture injected service is not the component injected service',
//     inject([HeroDetailService], (service: HeroDetailService) => {

//     expect(service).toEqual({}, 'service injected from fixture');
//     expect(hds).toBeTruthy('service injected into component');
//   }));
// }
