import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AngularFire } from 'angularfire2';

import { Post } from '../shared/post';
import { PostService } from '../shared/post.service';
import { User } from '../shared/user';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})

export class BlogComponent implements OnInit {
  posts: Post[];
  userId: number = null;
  isSignedIn: boolean = false;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private postService: PostService,
    private userService: UserService,
    private af: AngularFire) {}

  getPosts(): void {
    this.postService.getLimitPosts(null, 3).subscribe(posts => {
      this.posts = posts;
    });
  }

  ngOnInit() {
    this.getPosts();
    this.af.auth.subscribe((auth) => {
      if (auth) {
        this.userService.checkUser(`${auth.auth.email}`)
          .subscribe((user) => {
            if (user.length) {
              this.isSignedIn = true;
              this.userId = user[0].id
            } else {
              this.isSignedIn = false;
            }
         }); 
      }
    });
  }

  onSignOff() {
    this.af.auth.logout();
    this.isSignedIn = false;
    this.router.navigate(['/']);
  }

  goToPostDetail(post: Post): void {
    this.router.navigate(['./post', post.id], { relativeTo: this.route });
  }

  goToUserDetail(): void {
    this.router.navigate(['./user', this.userId], { relativeTo: this.route });
  }
}
