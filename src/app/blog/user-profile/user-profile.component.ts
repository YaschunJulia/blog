import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FirebaseObjectObservable, AngularFire } from 'angularfire2';


import { User } from '../../shared/user';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
  user : User;
  users: FirebaseObjectObservable<User[]>;
  key: number;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private af: AngularFire,
  ) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = params['id'];
      let email;
      this.af.auth.subscribe(auth => (auth) ? email = auth.auth.email : email = '');
      this.userService.getUser(id)
      .subscribe(snapshot => {
        this.user = snapshot.val();
        if (this.user.email != email) {
          delete this.user.firstName;
          delete this.user.lastName;
          delete this.user.email;
          delete this.user.password; 
        }      
      });
    }).catch(() => this.user = new User())
  }

}
