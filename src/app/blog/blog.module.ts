import { NgModule } from '@angular/core';
import { BlogComponent } from './blog.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { BlogRoutingModule } from './blog-routing.module';
import { SelectModule } from 'ng2-select/components/select.module';

import { PostsListComponent } from './posts-list/posts-list.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { RegistrationComponent } from './registration/registration.component';
import { InputWrapperComponent } from '../shared/input-wrapper/input-wrapper.component';

import { PostService } from '../shared/post.service';
import { UserService } from '../shared/user.service';

import { EqualValidator } from '../shared/validators/equal-validator.directive'; 
import { CorrectYearValidator } from '../shared/validators/correct-year-validator.directive';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BlogRoutingModule,
    InfiniteScrollModule,
    SelectModule
  ],
  declarations: [
    BlogComponent, 
    PostsListComponent, 
    PostDetailComponent,
    UserProfileComponent,
    RegistrationComponent,
    CorrectYearValidator,
    EqualValidator,
    InputWrapperComponent
  ],
  providers: [ 
    PostService,
    UserService
  ],
})

export class BlogModule {}