import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BlogComponent } from './blog.component';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { RegistrationComponent } from './registration/registration.component';

import { AuthGuard } from '../shared/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      {  
        path: 'blog',
        component: BlogComponent,
        children: [
          {
            path: '', 
            component: PostsListComponent,
          },
          {
            path: 'post/:id',
            component: PostDetailComponent,
          },
          {
            path: 'user/:id',
            component: UserProfileComponent,
          },
          {
            path: 'signup', 
            component: RegistrationComponent,
          },
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class BlogRoutingModule {}